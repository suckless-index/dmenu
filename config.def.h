/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1; /* -b option; if 0, dmenu appears at bottom */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"monospace:size=10"
};
static const char *prompt = 0; /* -p option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
	/* fg  bg */
	[SchemeNorm] = { "#bbbbbb", "#222222" },
	[SchemeSel] = { "#eeeeee", "#005577" },
	[SchemeOut] = { "#000000", "#00ffff" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines = 0;

/* Xresources */
Resource resources[] = {
	{ "topbar", INTEGER, &topbar },
	{ "font", STRING, &fonts[0] },
	{ "prompt", STRING, &prompt },
	{ "cnormfg", STRING, &colors[0][0] },
	{ "cnormbg", STRING, &colors[0][1] },
	{ "cselfg", STRING, &colors[1][0] },
	{ "cselbg", STRING, &colors[1][1] },
	{ "coutfg", STRING, &colors[2][0] },
	{ "coutbg", STRING, &colors[2][1] },
	{ "lines", INTEGER, &lines },
};

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
